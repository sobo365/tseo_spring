package tseo.tseoproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TseoProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(TseoProjectApplication.class, args);
	}
}
