package tseo.tseoproject.exam.dto;

import lombok.Getter;

@Getter
public class ExamCreateDTO {

    private Integer pointsExam;
    private Integer pointsLab;
    private Integer grade;
    private Long courseId;
    private String studentId;

    public ExamCreateDTO() {
		this.pointsExam = 0;
		this.pointsLab = 0;
		this.grade = 0;
		this.courseId = 0L;
		this.studentId = "";
	}
    
    public ExamCreateDTO(Integer pointsExam, Integer pointsLab, Integer grade, Long courseId, String studentId) {
        this.pointsExam = pointsExam;
        this.pointsLab = pointsLab;
        this.grade = grade;
        this.courseId = courseId;
        this.studentId = studentId;
    }

	public ExamCreateDTO(Long courseId, String studentId) {
		this.courseId = courseId;
		this.studentId = studentId;
	}
}
