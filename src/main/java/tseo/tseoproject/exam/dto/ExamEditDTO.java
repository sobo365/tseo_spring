package tseo.tseoproject.exam.dto;

import lombok.Getter;
import tseo.tseoproject.exam.model.Exam;

@Getter
public class ExamEditDTO {

    private Long id;
    private Integer pointsExam;
    private Integer pointsLab;
    private Integer grade;

    public ExamEditDTO(Long id, Integer pointsExam, Integer pointsLab, Integer grade) {
        this.id = id;
        this.pointsExam = pointsExam;
        this.pointsLab = pointsLab;
        this.grade = grade;
    }

    public Exam asEntity() {
        return new Exam(id, pointsLab, pointsExam, grade);
    }
}
