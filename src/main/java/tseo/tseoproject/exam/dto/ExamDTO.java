package tseo.tseoproject.exam.dto;

import lombok.Getter;
import tseo.tseoproject.course.dto.CourseDTO;
import tseo.tseoproject.student.dto.StudentDTO;

@Getter
public class ExamDTO {

    private Long id;
    private Integer pointsExam;
    private Integer pointsLab;
    private Integer grade;
    private CourseDTO courseDTO;
    private StudentDTO studentDTO;

    public ExamDTO(Long id, Integer pointsExam, Integer pointsLab, Integer grade, CourseDTO courseDTO, StudentDTO studentDTO) {
        this.id = id;
        this.pointsExam = pointsExam;
        this.pointsLab = pointsLab;
        this.grade = grade;
        this.courseDTO = courseDTO;
        this.studentDTO = studentDTO;
    }
}
