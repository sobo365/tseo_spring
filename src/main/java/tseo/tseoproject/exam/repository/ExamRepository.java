package tseo.tseoproject.exam.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tseo.tseoproject.exam.model.Exam;

@Repository
public interface ExamRepository extends JpaRepository<Exam, Long>{

}
