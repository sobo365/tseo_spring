package tseo.tseoproject.exam.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tseo.tseoproject.course.model.Course;
import tseo.tseoproject.course.service.CourseService;
import tseo.tseoproject.exam.dto.ExamCreateDTO;
import tseo.tseoproject.exam.dto.ExamDTO;
import tseo.tseoproject.exam.dto.ExamEditDTO;
import tseo.tseoproject.exam.model.Exam;
import tseo.tseoproject.exam.service.ExamService;
import tseo.tseoproject.student.model.Student;
import tseo.tseoproject.student.service.StudentService;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/exams")
@CrossOrigin
public class ExamController {
    private ExamService examService;
    private StudentService studentService;
    private CourseService courseService;

    public ExamController(ExamService examService, StudentService studentService, CourseService courseService) {
        this.examService = examService;
        this.studentService = studentService;
        this.courseService = courseService;
    }

    @GetMapping()
    public ResponseEntity<List<ExamDTO>> getAll() {
        List<Exam> exams = examService.getAll();
        List<ExamDTO> examDTOS = new ArrayList<>();

        for (Exam e : exams) {
            examDTOS.add(e.asDTO());
        }
        return ResponseEntity.ok(examDTOS);
    }

    @GetMapping("/{id}")
    public ResponseEntity<ExamDTO> getAll(@PathVariable Long id) {
        Exam exam = examService.get(id);
        return ResponseEntity.ok(exam.asDTO());
    }


    @PostMapping()
    public ResponseEntity<ExamDTO> insert(@RequestBody ExamCreateDTO examCreateDTO) throws Exception {
        Student student = studentService.get(UUID.fromString(examCreateDTO.getStudentId()));
        Course course = courseService.get(examCreateDTO.getCourseId());
        Exam exam = examService.insert(new Exam(examCreateDTO.getPointsLab(), examCreateDTO.getPointsExam(), examCreateDTO.getGrade(), course, student));
        return ResponseEntity.ok(exam.asDTO());
    }

    @PutMapping()
    public ResponseEntity<ExamDTO> edit(@RequestBody ExamEditDTO examEditDTO) {
        Exam exam = examService.get(examEditDTO.getId());
        exam.setPointsExam(examEditDTO.getPointsExam());
        exam.setPointsLab(examEditDTO.getPointsLab());
        exam.setGrade(examEditDTO.getGrade());
        Exam edited = examService.edit(exam);
        return ResponseEntity.ok(edited.asDTO());
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        examService.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
