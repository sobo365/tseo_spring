package tseo.tseoproject.exam.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import tseo.tseoproject.exam.dto.ExamDTO;
import tseo.tseoproject.student.model.Student;
import tseo.tseoproject.course.model.Course;

@Getter
@ToString
@Entity
@Setter
public class Exam {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	private Integer pointsExam;
    private Integer pointsLab;
    private Integer grade;
    private boolean active;
    
    @ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
	private Course course;
    
    @ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
	private Student student;

	public Exam() {
	}

	public Exam(Long id, Integer pointsExam, Integer pointsLab, Integer grade) {
		this.id = id;
		this.pointsExam = pointsExam;
		this.pointsLab = pointsLab;
		this.grade = grade;
	}

	public Exam(Integer pointsExam, Integer pointsLab, Integer grade, Course course, Student student) {
		this.pointsExam = pointsExam;
		this.pointsLab = pointsLab;
		this.grade = grade;
		this.course = course;
		this.student = student;
	}

	public ExamDTO asDTO() {
		return new ExamDTO(id, pointsExam, pointsLab, grade, course.asDTO(), student.asDTO());
	}
	
	public void delete() {
        this.active = false;
    }

}
