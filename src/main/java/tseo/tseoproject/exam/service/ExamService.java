package tseo.tseoproject.exam.service;

import java.util.List;

import tseo.tseoproject.exam.model.Exam;

public interface ExamService {
    List<Exam> getAll();

    Exam get(Long id);

    Exam insert(Exam exam);

    Exam edit(Exam exam);

    void delete(Long id);
}
