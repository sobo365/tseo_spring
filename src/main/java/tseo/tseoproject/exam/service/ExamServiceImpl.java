package tseo.tseoproject.exam.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;
import tseo.tseoproject.exam.exception.ExamNotFoundException;
import tseo.tseoproject.exam.model.Exam;
import tseo.tseoproject.exam.repository.ExamRepository;

@Service
public class ExamServiceImpl implements ExamService {

    private ExamRepository examRepository;

    public ExamServiceImpl(ExamRepository examRepository) {
        this.examRepository = examRepository;
    }

    @Override
    public List<Exam> getAll() {
    	List<Exam> exams = examRepository.findAll();
        List<Exam> active = new ArrayList<>();
        for (Exam e : exams) {
            if (e.isActive())
                active.add(e);
        }
        return active;
    }

    @Override
    public Exam get(Long id) {
        Optional<Exam> exam = examRepository.findById(id);
        if (!exam.isPresent() || !exam.get().isActive())
            throw new ExamNotFoundException("Exam with provided id does not exists!");

        return exam.get();
    }

    @Override
    public Exam insert(Exam exam) {
    	exam.setActive(true);
        return examRepository.save(exam);
    }

    @Override
    public Exam edit(Exam exam) {
        if (!examRepository.existsById(exam.getId()))
            throw new ExamNotFoundException("Exam with provided id does not exists!");
        exam.setActive(true);
        return examRepository.save(exam);
    }

    @Override
    public void delete(Long id) {
    	Optional<Exam> exam = examRepository.findById(id);
        if (!exam.isPresent())
            throw new ExamNotFoundException("Exam with provided id does not exists!");
        exam.get().delete();
        examRepository.save(exam.get());
    }
}
