package tseo.tseoproject.documents.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import org.dom4j.DocumentException;
import org.springframework.stereotype.Service;

import tseo.tseoproject.documents.dto.DocumentDTO;
import tseo.tseoproject.documents.dto.StudentDocumentCreateDTO;
import tseo.tseoproject.documents.model.Document;
import tseo.tseoproject.documents.repository.DocumentRepository;
import tseo.tseoproject.student.model.Student;
import tseo.tseoproject.student.service.StudentService;


@Service
public class DocumentServicelmpl implements DocumentService {

    private DocumentRepository documentRepository;
    private StudentService studentService;

    public DocumentServicelmpl(DocumentRepository documentRepository, StudentService studentService) {
        this.documentRepository = documentRepository;
        this.studentService = studentService;
    }

    @Override
    public List<Document> getAll() {
        return null;
    }

    @Override
    public Document get(Long id) {
        return null;
    }

    @Override
    public Document insert(StudentDocumentCreateDTO studentDocumentCreateDTO) {
        Student student = studentService.get(UUID.fromString(studentDocumentCreateDTO.getStudentId()));
        Document d = new Document(studentDocumentCreateDTO.getName(), studentDocumentCreateDTO.getData());
        d.setStudent(student);
        return documentRepository.save(d);
    }

    @Override
    public Document edit(Document lecture) {
        return null;
    }

    @Override
    public Document delete(Long id) {
        return null;
    }

    @Override
    public List<DocumentDTO> getByStudentId(String id) {
        Student student = studentService.get(UUID.fromString(id));
        Set<Document> documents = documentRepository.findAllByStudent(student);
        List<DocumentDTO> dtos = new ArrayList<>();
        documents.stream().forEach(d -> dtos.add(d.asDTO()));
        return dtos;
    }
}
