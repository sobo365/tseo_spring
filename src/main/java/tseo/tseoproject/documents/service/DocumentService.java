package tseo.tseoproject.documents.service;

import java.util.List;

import org.dom4j.DocumentException;
import tseo.tseoproject.documents.dto.DocumentDTO;
import tseo.tseoproject.documents.dto.StudentDocumentCreateDTO;
import tseo.tseoproject.documents.model.Document;
import tseo.tseoproject.student.model.Student;


public interface DocumentService {
    List<Document> getAll();

    Document insert(StudentDocumentCreateDTO studentDocumentCreateDTO) throws DocumentException;

    Document edit(Document lecture);

    Document get(Long id);

    Document delete(Long id);

    List<DocumentDTO> getByStudentId(String id);

}
