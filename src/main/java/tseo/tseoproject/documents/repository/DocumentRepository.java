package tseo.tseoproject.documents.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.stereotype.Repository;
import tseo.tseoproject.documents.model.Document;
import tseo.tseoproject.student.model.Student;

import java.util.Set;

@Repository
public interface DocumentRepository extends JpaRepository<Document, Long> {
    Set<Document> findAllByStudent(Student student);
}
