package tseo.tseoproject.documents.model;

import javax.persistence.*;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import tseo.tseoproject.student.model.Student;
import tseo.tseoproject.documents.dto.DocumentDTO;


@Getter
@ToString
@Entity
@Setter
public class Document {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;

    @Column(name = "data", unique = false, nullable = false, columnDefinition = "LONGTEXT")
    private String data;

    @ManyToOne
    @JoinColumn(name = "studentId")
    private Student student;

    public Document() {
    }

    public Document(Long id, String name, String data) {
        this.id = id;
        this.name = name;
        this.data = data;
    }

    public Document(String name, String data) {
        this.name = name;
        this.data = data;
    }

    public DocumentDTO asDTO() {
        return new DocumentDTO(id, name, data);
    }
} 
 