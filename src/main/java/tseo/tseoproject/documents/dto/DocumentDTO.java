package tseo.tseoproject.documents.dto;

import lombok.Getter;
import lombok.Setter;
import tseo.tseoproject.documents.model.Document;

@Getter
@Setter
public class DocumentDTO {
    private Long id;
    private String name;
    private String path;

    public DocumentDTO() {
    }

    public DocumentDTO(Long id, String name, String path) {
        super();
        this.id = id;
        this.name = name;
        this.path = path;
    }

    public Document asEntity() {
        return new Document(id, name, path);
    }


}
