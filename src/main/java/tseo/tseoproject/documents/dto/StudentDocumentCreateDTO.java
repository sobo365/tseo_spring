package tseo.tseoproject.documents.dto;

import lombok.Getter;
import lombok.ToString;
import tseo.tseoproject.documents.model.Document;


@Getter
@ToString
public class StudentDocumentCreateDTO {

    private String studentId;
    private String name;
    private String data;

    public StudentDocumentCreateDTO() {
    }

    public StudentDocumentCreateDTO(String name, String data, String studentId) {
        this.name = name;
        this.data = data;
        this.studentId = studentId;
    }

}
