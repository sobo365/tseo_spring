package tseo.tseoproject.documents.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class StudentDocumentNotFoundException extends RuntimeException {
    public StudentDocumentNotFoundException(String message) {
        super(message);
    }
}
