package tseo.tseoproject.documents.controller;

import java.util.List;

import org.dom4j.DocumentException;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import tseo.tseoproject.documents.dto.StudentDocumentCreateDTO;
import tseo.tseoproject.documents.dto.DocumentDTO;
import tseo.tseoproject.documents.model.Document;
import tseo.tseoproject.documents.service.DocumentService;

@RestController
@RequestMapping("/documents")
@CrossOrigin
public class StudentDocumentController {

    private DocumentService documentService;

    public StudentDocumentController(DocumentService documentService) {
        this.documentService = documentService;
    }

    @GetMapping("/{student_id}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_TEACHER', 'ROLE_STUDENT')")
    public ResponseEntity<List<DocumentDTO>> getStudentDocuments(@PathVariable String student_id) {
        List<DocumentDTO> ret = documentService.getByStudentId(student_id);
        return ResponseEntity.ok(ret);
    }

    @PostMapping()
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_TEACHER', 'ROLE_STUDENT')")
    public ResponseEntity<DocumentDTO> insert(@RequestBody StudentDocumentCreateDTO studentDocumentCreateDTO) throws DocumentException {
        Document document = documentService.insert(studentDocumentCreateDTO);
        return ResponseEntity.ok(document.asDTO());
    }

    @PutMapping()
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_TEACHER', 'ROLE_STUDENT')")
    public ResponseEntity<DocumentDTO> edit(@RequestBody DocumentDTO documentDTO) {
        Document document = documentService.edit(documentDTO.asEntity());
        return ResponseEntity.ok(document.asDTO());
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_TEACHER', 'ROLE_STUDENT')")
    public ResponseEntity<String> delete(@PathVariable Long id) {
        documentService.delete(id);
        return ResponseEntity.ok("Student document deleted!");
    }

}
