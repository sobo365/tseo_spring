package tseo.tseoproject.security;

public class SecurityConstants {
    public static final long ACCESS_TOKEN_VALIDITY_SECONDS = 5 * 60 * 60;
    public static final String SIGNING_KEY = "3M4gYSEtsLLRhGpQGsWAGArao3wJcv6JiU9KH6TBuFbxxtvZyk0JIMyRKalT8Z0fUtbdZnzuyzfgJqFeI3wVFTQZVke2GEuR6JgRTNZync7iQmHdVkjPuUECsHMOcvHJnAN2FeO6XuVJbF0usvVdla46OfZh6W1JqSyxMh7gFkb0pmphbajpqqGrzR8Q8unlAcIBxo1UDJul4SyBqPKb2PSho1Bj2ZD9014WcKmhGYdU9Qbvx1o0nFlr6oiDDfIpwXvsiLC6NdxSXbO6xq4nc29c5aMX2gYVe7KSVKue";
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";
    public static final String AUTHORITIES_KEY = "scopes";
}
