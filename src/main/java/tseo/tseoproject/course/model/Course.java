package tseo.tseoproject.course.model;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import tseo.tseoproject.course.dto.CourseDTO;
import tseo.tseoproject.enrollment.model.Enrollment;
import tseo.tseoproject.exam.model.Exam;
import tseo.tseoproject.lecture.Lecture;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Getter
@ToString
@Entity
@Setter
public class Course {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    @OneToMany(mappedBy = "course", fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    private Set<Enrollment> enrollments = new HashSet<>();

    @OneToMany(mappedBy = "course", fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    private Set<Exam> exams = new HashSet<Exam>();

    @OneToMany(mappedBy = "course", fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    private List<Lecture> lectures = new ArrayList<>();

    private boolean active;

    public Course() {
    }

    public Course(String name) {
        this.name = name;
    }

    public Course(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public CourseDTO asDTO() {
        return new CourseDTO(id.toString(), name);
    }

    public void delete() {
        this.active = false;
    }

}
