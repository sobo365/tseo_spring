package tseo.tseoproject.course.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import tseo.tseoproject.course.dto.CourseCreateDTO;
import tseo.tseoproject.course.dto.CourseDTO;
import tseo.tseoproject.course.model.Course;
import tseo.tseoproject.course.service.CourseService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

@RestController
@RequestMapping("/courses")
@CrossOrigin()
public class CourseController {

    private CourseService courseService;

    public CourseController(CourseService courseService) {
        this.courseService = courseService;
    }

    @GetMapping()
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_TEACHER')")
    public ResponseEntity<List<CourseDTO>> getAll() {
        List<Course> courses = courseService.getAll();
        List<CourseDTO> courseDTOs = new ArrayList<>();

        courses.stream().forEach(c -> courseDTOs.add(c.asDTO()));
        return ResponseEntity.ok(courseDTOs);
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_TEACHER', 'ROLE_STUDENT')")
    public ResponseEntity<CourseDTO> get(@PathVariable Long id) throws Exception {
        Course course = courseService.get(id);
        return ResponseEntity.ok(course.asDTO());
    }

    @PostMapping()
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<CourseDTO> insert(@RequestBody CourseCreateDTO courseCreateDTO) {
        Course course = courseService.insert(courseCreateDTO.asEntity());
        return ResponseEntity.ok(course.asDTO());
    }

    @PutMapping()
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<CourseDTO> edit(@RequestBody CourseDTO courseDTO) {
        Course course = courseService.edit(courseDTO.asEntity());
        return ResponseEntity.ok(course.asDTO());
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<String> delete(@PathVariable Long id) {
        courseService.delete(id);
        return ResponseEntity.ok("Course deleted!");
    }
}
