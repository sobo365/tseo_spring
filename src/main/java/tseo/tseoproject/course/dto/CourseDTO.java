package tseo.tseoproject.course.dto;

import lombok.Getter;
import tseo.tseoproject.course.model.Course;

@Getter
public class CourseDTO {
    private String id;
    private String name;

    public CourseDTO() {
    }

    public CourseDTO(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public Course asEntity() {
        return new Course(Long.parseLong(id), name);
    }
}
