package tseo.tseoproject.course.dto;

import lombok.Getter;
import tseo.tseoproject.course.model.Course;

@Getter
public class CourseCreateDTO {

    private String name;

    public CourseCreateDTO() {
    }

    public CourseCreateDTO(String name) {
        this.name = name;
    }

    public Course asEntity() {
        return new Course(name);
    }
}
