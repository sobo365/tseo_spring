package tseo.tseoproject.course.service;


import org.springframework.stereotype.Service;
import tseo.tseoproject.course.exception.CourseNotFoundException;
import tseo.tseoproject.course.model.Course;
import tseo.tseoproject.course.repository.CourseRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CourseServiceImpl implements CourseService {

    private CourseRepository courseRepository;

    public CourseServiceImpl(CourseRepository courseRepository) {
        this.courseRepository = courseRepository;
    }

    @Override
    public List<Course> getAll() {
        return courseRepository.findByActive(true);
    }

    @Override
    public Course get(Long id) {
        Optional<Course> course = courseRepository.findById(id);
        if (!course.isPresent() || !course.get().isActive())
            throw new CourseNotFoundException("Course with provided id does not exists!");
        return course.get();
    }

    @Override
    public Course insert(Course course) {
        course.setActive(true);
        return courseRepository.save(course);
    }

    @Override
    public Course edit(Course course) {
        if (!courseRepository.existsById(course.getId()))
            throw new CourseNotFoundException("Course with provided id does not exists!");
        course.setActive(true);
        return courseRepository.save(course);
    }

    @Override
    public void delete(Long id) {
        Optional<Course> course = courseRepository.findById(id);
        if (!course.isPresent())
            throw new CourseNotFoundException("Course with provided id does not exists!");
        course.get().delete();
        courseRepository.save(course.get());
    }
}
