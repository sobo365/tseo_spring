package tseo.tseoproject.course.service;

import tseo.tseoproject.course.model.Course;

import java.util.List;

public interface CourseService {

    List<Course> getAll();

    Course get(Long id) throws Exception;

    Course insert(Course course);

    Course edit(Course course);

    void delete(Long id);
}
