package tseo.tseoproject.lecture;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import tseo.tseoproject.lecture.dto.RoleDTO;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Getter
@Setter
@Entity
@ToString
public class Role {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    public Role() {
    }

    public Role(String name) {
        this.name = name;
    }

    public RoleDTO asDTO() {
        return new RoleDTO(id, name);
    }
}
