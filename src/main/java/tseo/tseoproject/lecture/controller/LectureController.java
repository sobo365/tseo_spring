package tseo.tseoproject.lecture.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import tseo.tseoproject.course.model.Course;
import tseo.tseoproject.course.service.CourseService;
import tseo.tseoproject.lecture.Lecture;
import tseo.tseoproject.lecture.Role;
import tseo.tseoproject.lecture.dto.LectureCreateDTO;
import tseo.tseoproject.lecture.dto.LectureDTO;
import tseo.tseoproject.lecture.dto.LectureEditDTO;
import tseo.tseoproject.lecture.dto.RoleDTO;
import tseo.tseoproject.lecture.service.LectureService;
import tseo.tseoproject.lecture.service.RoleService;
import tseo.tseoproject.teacher.Teacher;
import tseo.tseoproject.teacher.service.TeacherService;

@RestController
@RequestMapping("/lectures")
@CrossOrigin(origins = "http://localhost:4200")
public class LectureController {

	private LectureService lectureService;
	private CourseService courseService;
	private TeacherService teacherService;
	private RoleService roleService;
	
	public LectureController(LectureService lectureService, CourseService courseService,
			TeacherService teacherService, RoleService roleService) {
		this.lectureService = lectureService;
		this.courseService = courseService;
		this.teacherService = teacherService;
		this.roleService = roleService;
	}
	
	@GetMapping()
	@PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_TEACHER')")
    public ResponseEntity<List<LectureDTO>> getAll() {
        List<Lecture> lectures = lectureService.getAll();
        List<LectureDTO> lectureDTOS = new ArrayList<>();

        for (Lecture l : lectures) {
            lectureDTOS.add(l.asDTO());
        }
        return ResponseEntity.ok(lectureDTOS);
    }
	
	@GetMapping("/{id}")
    public ResponseEntity<LectureDTO> getAll(@PathVariable Long id) {
        Lecture lecture = lectureService.get(id);
        return ResponseEntity.ok(lecture.asDTO());
    }
	
	@PostMapping()
    public ResponseEntity<LectureDTO> insert(@RequestBody LectureCreateDTO lectureCreateDTO) throws Exception {
	    Course course = courseService.get(lectureCreateDTO.getCourseId());
	    Teacher teacher = teacherService.get(lectureCreateDTO.getTeacherId());
	    Role role = roleService.getRole(lectureCreateDTO.getRoleId());
	    Lecture lecture = lectureService.insert(new Lecture(course, teacher, role));
	    return ResponseEntity.ok(lecture.asDTO());
    }
	
	@PutMapping()
    public ResponseEntity<LectureDTO> edit(@RequestBody LectureEditDTO lectureEditDTO) {
        Lecture lecture = lectureService.get(lectureEditDTO.getId());
//        lecture.setRole(lectureEditDTO.getRole());
        Lecture edited = lectureService.edit(lecture);
        return ResponseEntity.ok(edited.asDTO());
    }
	
	@DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        lectureService.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/roles")
    public ResponseEntity<List<RoleDTO>> getRoles(){
	    List<Role> roles = roleService.getAll();
	    List<RoleDTO> dtos = new ArrayList<>();
	    roles.stream().forEach(r -> dtos.add(r.asDTO()));
	    return ResponseEntity.ok(dtos);
    }
}
