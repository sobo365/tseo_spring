package tseo.tseoproject.lecture.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import tseo.tseoproject.lecture.Lecture;

public interface LectureRepository extends JpaRepository<Lecture, Long> {
}
