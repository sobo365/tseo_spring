package tseo.tseoproject.lecture;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import tseo.tseoproject.course.model.Course;
import tseo.tseoproject.lecture.dto.LectureDTO;
import tseo.tseoproject.teacher.Teacher;

@Setter
@Getter
@ToString
@Entity
public class Lecture {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;

	@OneToOne(fetch = FetchType.EAGER)
    private Role role;
	
	@ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
	private Course course;
	
	@ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
	private Teacher teachers;
	
	private boolean active;
	
	public Lecture () {
		
	}

	public Lecture(Course course, Teacher teachers) {
		this.course = course;
		this.teachers = teachers;
	}

	public Lecture(Course course, Teacher teachers, Role role) {
		this.role = role;
		this.course = course;
		this.teachers = teachers;
	}

	public Lecture(Long id) {
		this.id = id;
//		this.role = role;
	}
	
	public LectureDTO asDTO() {
		return new LectureDTO(id, course.asDTO(), teachers.asDTO(), role.asDTO());
	}
	
	public void delete() {
		this.active = false;
	}
}
