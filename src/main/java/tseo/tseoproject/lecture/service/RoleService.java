package tseo.tseoproject.lecture.service;

import org.springframework.stereotype.Service;
import tseo.tseoproject.lecture.Role;
import tseo.tseoproject.lecture.exception.RoleNotFoundException;
import tseo.tseoproject.lecture.repository.RoleRepository;

import java.util.List;

@Service
public class RoleService {

    private RoleRepository roleRepository;

    public RoleService(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    public List<Role> getAll() {
        return roleRepository.findAll();
    }

    public Role getRole(Long id) {
        if(!roleRepository.existsById(id)) {
            throw new RoleNotFoundException("Role with provided id does not exists");
        }
        return roleRepository.getOne(id);
    }
}
