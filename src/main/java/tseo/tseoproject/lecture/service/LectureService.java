package tseo.tseoproject.lecture.service;

import java.util.List;
import tseo.tseoproject.lecture.Lecture;
import tseo.tseoproject.teacher.Teacher;

public interface LectureService {

	List<Lecture> getAll();
	Lecture get(Long id);
	Lecture insert(Lecture lecture);
	Lecture edit(Lecture lecture);
	void delete(Long id);
}
