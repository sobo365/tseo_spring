package tseo.tseoproject.lecture.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;
import tseo.tseoproject.lecture.Lecture;
import tseo.tseoproject.lecture.exception.LectureNotFoundException;
import tseo.tseoproject.lecture.repository.LectureRepository;
import tseo.tseoproject.teacher.Teacher;

@Service
public class LectureServiceImpl implements LectureService {
	
	private LectureRepository lectureRepository;

	public LectureServiceImpl(LectureRepository lectureRepository) {
		this.lectureRepository = lectureRepository;
	}

	@Override
	public List<Lecture> getAll() {
		List<Lecture> lectures = lectureRepository.findAll();
		List<Lecture> active = new ArrayList<>();
		for (Lecture l : lectures) {
			if (l.isActive())
				active.add(l);
		}
		return active;
	}
	
	@Override
    public Lecture get(Long id) {
        Optional<Lecture> lecture = lectureRepository.findById(id);
        if (!lecture.isPresent() || lecture.get().isActive())
            throw new LectureNotFoundException("Lecture with provided id does not exists!");
        return lecture.get();
    }

	@Override
	public Lecture insert(Lecture lecture) {
		lecture.setActive(true);
		return lectureRepository.save(lecture);
	}

	@Override
    public Lecture edit(Lecture lecture) {
        if (!lectureRepository.existsById(lecture.getId()))
            throw new LectureNotFoundException("Lecture with provided id does not exists!");
        lecture.setActive(true);
        return lectureRepository.save(lecture);
    }

	@Override
    public void delete(Long id) {
		Optional<Lecture> lecture = lectureRepository.findById(id);
        if (!lecture.isPresent())
            throw new LectureNotFoundException("Lecture with provided id does not exists!");
        lecture.get().delete();
        lectureRepository.save(lecture.get());
    }	
}
