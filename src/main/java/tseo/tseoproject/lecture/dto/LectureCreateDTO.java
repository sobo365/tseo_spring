package tseo.tseoproject.lecture.dto;

import lombok.Getter;
import tseo.tseoproject.lecture.Role;

@Getter
public class LectureCreateDTO {

    private Long courseId;
    private String teacherId;
    private Long roleId;

    public LectureCreateDTO(Long courseId, String teacherId, Long roleId) {
        this.courseId = courseId;
        this.teacherId = teacherId;
        this.roleId = roleId;
    }
}
