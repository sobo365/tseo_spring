package tseo.tseoproject.lecture.dto;


import lombok.Getter;
import tseo.tseoproject.course.dto.CourseDTO;
import tseo.tseoproject.lecture.Role;
import tseo.tseoproject.teacher.dto.TeacherDTO;

@Getter
public class LectureDTO {

	private Long id;
	private CourseDTO courseDTO;
	private TeacherDTO teacherDTO;
	private RoleDTO roleDTO;
	
	public LectureDTO(Long id, CourseDTO courseDTO, TeacherDTO teacherDTO, RoleDTO roleDTO) {
		this.id = id;
		this.courseDTO = courseDTO;
		this.teacherDTO = teacherDTO;
		this.roleDTO = roleDTO;
	}
}
