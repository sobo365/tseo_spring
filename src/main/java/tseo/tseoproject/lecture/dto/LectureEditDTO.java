package tseo.tseoproject.lecture.dto;

import lombok.Getter;
import tseo.tseoproject.lecture.Lecture;
import tseo.tseoproject.lecture.Role;

@Getter
public class LectureEditDTO {

	private Long id;
	private Role role;
	
	public LectureEditDTO(Long id) {
		this.id = id;
//		this.role = role;
	}
	
	public Lecture asEntity() {
		return new Lecture(id);
	}
}
