package tseo.tseoproject.student.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import tseo.tseoproject.student.dto.StudentCreateDTO;
import tseo.tseoproject.student.dto.StudentDTO;
import tseo.tseoproject.student.model.Student;
import tseo.tseoproject.student.service.StudentService;

@RestController
@RequestMapping("/students")
@CrossOrigin
public class StudentController {
    private StudentService studentService;

    public StudentController(StudentService studentService) {
        this.studentService = studentService;
    }

    @GetMapping()
    public ResponseEntity<List<StudentDTO>> getAll() {
        List<Student> students = studentService.getAll();
        List<StudentDTO> studentsDTO = new ArrayList<>();

        for (Student s : students) {
            studentsDTO.add(s.asDTO());
        }
        return ResponseEntity.ok(studentsDTO);
    }

    @GetMapping("/{id}")
    public ResponseEntity<StudentDTO> get(@PathVariable String id) throws Exception {
        Student student = studentService.get(UUID.fromString(id));
        return ResponseEntity.ok(student.asDTO());
    }

    @PostMapping()
    public ResponseEntity<StudentDTO> insert(@RequestBody StudentCreateDTO studentCreateDTO) {
        Student student = studentService.insert(studentCreateDTO.asEntity());
        return ResponseEntity.ok(student.asDTO());
    }

    @PutMapping()
    public ResponseEntity<StudentDTO> edit(@RequestBody StudentDTO studentDTO) {
        Student student = studentService.edit(studentDTO.asEntity());
        return ResponseEntity.ok(student.asDTO());
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> delete(@PathVariable String id) {
        studentService.delete(UUID.fromString(id));
        return ResponseEntity.ok("Student deleted!");
    }
}
