package tseo.tseoproject.student.dto;

import lombok.Getter;
import tseo.tseoproject.student.model.Student;

@Getter
public class StudentCreateDTO {
	private String firstName;
	private String lastName;
	private String cardNumber;
	
	public StudentCreateDTO() {
		
	}

	public StudentCreateDTO(String firstName, String lastName, String cardNumber) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.cardNumber = cardNumber;
	}
	
	public Student asEntity() {
		return new Student(firstName, lastName, cardNumber);
	}
}
