package tseo.tseoproject.student.dto;

import lombok.Getter;
import tseo.tseoproject.student.model.Student;
import tseo.tseoproject.documents.dto.DocumentDTO;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Getter
public class StudentDTO {
	private String id;
	private String firstName;
	private String lastName;
	private String cardNumber;
	private List<DocumentDTO> documentDTO = new ArrayList<>();

	public StudentDTO() {
		
	}

	public StudentDTO(String id, String firstName, String lastName, String cardNumber, List<DocumentDTO> documentDTO) {
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.cardNumber = cardNumber;
		this.documentDTO = documentDTO;
	}
	
	public Student asEntity() {
        return new Student(UUID.fromString(id), firstName, lastName, cardNumber);
    }
}
