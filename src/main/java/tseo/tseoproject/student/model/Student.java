package tseo.tseoproject.student.model;

import java.util.*;

import javax.persistence.*;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.Type;
import tseo.tseoproject.enrollment.model.Enrollment;
import tseo.tseoproject.exam.model.Exam;
import tseo.tseoproject.student.dto.StudentDTO;
import tseo.tseoproject.documents.dto.DocumentDTO;
import tseo.tseoproject.documents.model.Document;

@Getter
@ToString
@Entity
@Setter
public class Student {

    @Id
    @Type(type = "org.hibernate.type.UUIDCharType")
    private UUID id;
    private String firstName;
    private String lastName;
    private String cardNumber;
    private boolean active;

    @OneToMany(mappedBy = "student")
    private Set<Document> documents = new HashSet<>();
    @OneToMany(mappedBy = "student", fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    private Set<Enrollment> enrollments = new HashSet<Enrollment>();

    @OneToMany(mappedBy = "student", fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    private Set<Exam> exams = new HashSet<Exam>();

    public Student() {

    }

    public Student(String firstName, String lastName, String cardNumber) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.cardNumber = cardNumber;
    }

    public Student(UUID id, String firstName, String lastName, String cardNumber) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.cardNumber = cardNumber;
    }

    public void delete() {
        this.active = false;
    }

    public StudentDTO asDTO() {
        List<DocumentDTO> dtos = new ArrayList<>();

        documents.stream().forEach(d -> dtos.add(d.asDTO()));
        return new StudentDTO(id.toString(), firstName, lastName, cardNumber, dtos);
    }
}
