package tseo.tseoproject.student.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import tseo.tseoproject.student.model.Student;

import java.util.List;
import java.util.UUID;

public interface StudentRepository extends JpaRepository<Student, UUID>{
    public List<Student> findByActive(boolean active);
}
