package tseo.tseoproject.student.service;

import java.util.List;
import java.util.UUID;

import tseo.tseoproject.student.model.Student;

public interface StudentService {
	List<Student> getAll();
	Student get(UUID id);
	Student insert(Student student);
	Student edit(Student student);
	void delete(UUID id);
}
