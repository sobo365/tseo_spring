package tseo.tseoproject.student.service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.stereotype.Service;

import tseo.tseoproject.student.exception.StudentNotFoundException;
import tseo.tseoproject.student.model.Student;
import tseo.tseoproject.student.repository.StudentRepository;
import tseo.tseoproject.user.util.UserGenerator;

@Service
public class StudentServiceImpl implements StudentService {
    private StudentRepository studentRepository;
    private UserGenerator userGenerator;

    public StudentServiceImpl(StudentRepository studentRepository, UserGenerator userGenerator) {
        this.studentRepository = studentRepository;
        this.userGenerator = userGenerator;
    }

    @Override
    public List<Student> getAll() {
        return studentRepository.findByActive(true);
    }

    @Override
    public Student get(UUID id) {
        Optional<Student> student = studentRepository.findById(id);
        if (!student.isPresent() || !student.get().isActive())
            throw new StudentNotFoundException("Student with provided id does not exists!");
        return student.get();
    }

    @Override
    public Student insert(Student student) {
        student.setId(UUID.randomUUID());
        student.setActive(true);
        Student s = studentRepository.save(student);
        userGenerator.generateStudentAccount(s);
        return s;
    }

    @Override
    public Student edit(Student student) {
        if (!studentRepository.existsById(student.getId()))
            throw new StudentNotFoundException("Student with provided id does not exists!");
        return studentRepository.save(student);
    }

    @Override
    public void delete(UUID id) {
        Optional<Student> student = studentRepository.findById(id);
        if (!student.isPresent())
            throw new StudentNotFoundException("Student with provided id does not exists!");
        student.get().delete();
        studentRepository.save(student.get());
    }
}
