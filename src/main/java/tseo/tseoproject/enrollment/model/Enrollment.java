package tseo.tseoproject.enrollment.model;

import java.util.Date;

import javax.persistence.*;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import tseo.tseoproject.student.model.Student;
import tseo.tseoproject.teacher.Teacher;
import tseo.tseoproject.course.model.Course;
import tseo.tseoproject.enrollment.dto.EnrollmentDTO;

@Setter
@Getter
@ToString
@Entity
public class Enrollment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    private Date date;

    @ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
    private Course course;

    @ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
    private Student student;
    
    @ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
    private Teacher teacher;

    
    private boolean active;

    public Enrollment() {
    	
    }
    
    public Enrollment(Long id, Date date) {
		this.id = id;
		this.date = date;
	}
    
	public Enrollment(Date date, Course course, Student student, Teacher teacher) {
		this.date = date;
		this.course = course;
		this.student = student;
		this.teacher = teacher;
	}
	
	public EnrollmentDTO asDTO() {
		return new EnrollmentDTO(id, date, course.asDTO(), student.asDTO(), teacher.asDTO());
	}
	
	public void delete() {
        this.active = false;
    }
}
