package tseo.tseoproject.enrollment.service;

import java.util.List;

import tseo.tseoproject.enrollment.model.Enrollment;

public interface EnrollmentService {
	List<Enrollment> getAll();
	Enrollment get(Long id);
	Enrollment insert(Enrollment enrollment);
	Enrollment edit(Enrollment enrollment);
	void delete(Long id);
}
