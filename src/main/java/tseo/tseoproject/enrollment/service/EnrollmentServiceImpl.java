package tseo.tseoproject.enrollment.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import tseo.tseoproject.enrollment.exception.EnrollmentNotFoundException;
import tseo.tseoproject.enrollment.model.Enrollment;
import tseo.tseoproject.enrollment.repository.EnrollmentRepository;

@Service
public class EnrollmentServiceImpl implements EnrollmentService {
    private EnrollmentRepository enrollmentRepository;

    public EnrollmentServiceImpl(EnrollmentRepository enrollmentRepository) {
        this.enrollmentRepository = enrollmentRepository;
    }

    @Override
    public List<Enrollment> getAll() {
        List<Enrollment> enrollments = enrollmentRepository.findAll();
        List<Enrollment> active = new ArrayList<>();
        for (Enrollment e : enrollments) {
            if (e.isActive())
                active.add(e);
        }
        return active;

    }

    @Override
    public Enrollment get(Long id) {
        Optional<Enrollment> enrollment = enrollmentRepository.findById(id);
        if (!enrollment.isPresent() || !enrollment.get().isActive())
            throw new EnrollmentNotFoundException("Enrollment with provided id does not exists!");
        return enrollment.get();
    }

    @Override
    public Enrollment insert(Enrollment enrollment) {
    	enrollment.setActive(true);
        return enrollmentRepository.save(enrollment);
    }

    @Override
    public Enrollment edit(Enrollment enrollment) {
        if (!enrollmentRepository.existsById(enrollment.getId()))
            throw new EnrollmentNotFoundException("Enrollment with provided id does not exists!");
        enrollment.setActive(true);
        return enrollmentRepository.save(enrollment);
    }

    @Override
    public void delete(Long id) {
        Optional<Enrollment> enrollment = enrollmentRepository.findById(id);
        if (!enrollment.isPresent())
        	throw new EnrollmentNotFoundException("Enrollment with provided id does not exists!");
        enrollment.get().delete();
        enrollmentRepository.save(enrollment.get());

    }
}
