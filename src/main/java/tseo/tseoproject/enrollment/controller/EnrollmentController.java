package tseo.tseoproject.enrollment.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import tseo.tseoproject.course.model.Course;
import tseo.tseoproject.course.service.CourseService;
import tseo.tseoproject.enrollment.dto.EnrollmentCreateDTO;
import tseo.tseoproject.enrollment.dto.EnrollmentDTO;
import tseo.tseoproject.enrollment.dto.EnrollmentEditDTO;
import tseo.tseoproject.enrollment.model.Enrollment;
import tseo.tseoproject.enrollment.service.EnrollmentService;
import tseo.tseoproject.student.model.Student;
import tseo.tseoproject.student.service.StudentService;
import tseo.tseoproject.teacher.Teacher;
import tseo.tseoproject.teacher.service.TeacherService;

@RestController
@RequestMapping("/enrollments")
@CrossOrigin
public class EnrollmentController {
	private EnrollmentService enrollmentService;
    private StudentService studentService;
    private CourseService courseService;
    private TeacherService teacherService;
    
	public EnrollmentController(EnrollmentService enrollmentService, StudentService studentService,
			CourseService courseService, TeacherService teacherService) {
		this.enrollmentService = enrollmentService;
		this.studentService = studentService;
		this.courseService = courseService;
		this.teacherService = teacherService;
	}
    
	@GetMapping()
    public ResponseEntity<List<EnrollmentDTO>> getAll() {
        List<Enrollment> enrollments = enrollmentService.getAll();
        List<EnrollmentDTO> enrollmentsDTO = new ArrayList<>();
        for (Enrollment e : enrollments) {
        	enrollmentsDTO.add(e.asDTO());
        }
        return ResponseEntity.ok(enrollmentsDTO);
    }
	
	@GetMapping("/{id}")
    public ResponseEntity<EnrollmentDTO> getAll(@PathVariable Long id) {
        Enrollment enrollment = enrollmentService.get(id);
        return ResponseEntity.ok(enrollment.asDTO());
    }
	
	@PostMapping()
    public ResponseEntity<EnrollmentDTO> insert(@RequestBody EnrollmentCreateDTO enrollmentCreateDTO) throws Exception {
        Student student = studentService.get(UUID.fromString(enrollmentCreateDTO.getStudentId()));
        Course course = courseService.get(enrollmentCreateDTO.getCourseId());
        Teacher teacher = teacherService.get(enrollmentCreateDTO.getTeacherId());
        Enrollment enrollment = enrollmentService.insert(new Enrollment(enrollmentCreateDTO.getDate(), course, student, teacher));
        return ResponseEntity.ok(enrollment.asDTO());
    }
	
	@PutMapping()
    public ResponseEntity<EnrollmentDTO> edit(@RequestBody EnrollmentEditDTO enrollmentEditDTO) {
		Enrollment enrollment = enrollmentService.get(enrollmentEditDTO.getId());
		enrollment.setDate(enrollmentEditDTO.getDate());
		Enrollment editedEnrollment = enrollmentService.edit(enrollment);
        return ResponseEntity.ok(editedEnrollment.asDTO());
    }
	
	@DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
		enrollmentService.delete(id);
		return new ResponseEntity<>(HttpStatus.OK);
    }
}
