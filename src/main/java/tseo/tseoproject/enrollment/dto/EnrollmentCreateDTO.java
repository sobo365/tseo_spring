package tseo.tseoproject.enrollment.dto;

import java.util.Date;

import lombok.Getter;

@Getter
public class EnrollmentCreateDTO {
	private Date date;
	private Long courseId;
	private String studentId;
	private String teacherId;
	
	public EnrollmentCreateDTO(Date date, Long courseId, String studentId, String teacherId) {
		this.date = date;
		this.courseId = courseId;
		this.studentId = studentId;
		this.teacherId = teacherId;
	}
}
