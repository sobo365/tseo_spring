package tseo.tseoproject.enrollment.dto;

import java.util.Date;

import lombok.Getter;
import tseo.tseoproject.course.dto.CourseDTO;
import tseo.tseoproject.student.dto.StudentDTO;
import tseo.tseoproject.teacher.dto.TeacherDTO;

@Getter
public class EnrollmentDTO {
	private Long id;
	private Date date;
    private CourseDTO courseDTO;
    private StudentDTO studentDTO;
    private TeacherDTO teacherDTO;
    
	public EnrollmentDTO(Long id, Date date, CourseDTO courseDTO, StudentDTO studentDTO, TeacherDTO teacherDTO) {
		this.id = id;
		this.date = date;
		this.courseDTO = courseDTO;
		this.studentDTO = studentDTO;
		this.teacherDTO = teacherDTO;
	}
}
