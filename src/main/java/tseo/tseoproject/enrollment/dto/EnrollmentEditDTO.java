package tseo.tseoproject.enrollment.dto;

import java.util.Date;

import lombok.Getter;
import tseo.tseoproject.enrollment.model.Enrollment;

@Getter
public class EnrollmentEditDTO {
	private Long id;
	private Date date;
	
	public EnrollmentEditDTO(Long id, Date date) {
		this.id = id;
		this.date = date;
	}
	
	public Enrollment asEntity() {
        return new Enrollment(id, date);
    }
}
