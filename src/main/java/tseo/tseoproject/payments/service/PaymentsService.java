package tseo.tseoproject.payments.service;

import java.util.List;

import tseo.tseoproject.payments.model.Payments;


public interface PaymentsService {
    List<Payments> getAll();

    Payments get(Long id);

    Payments insert(Payments student);

    Payments edit(Payments student);

    Payments delete(Long id);
}
