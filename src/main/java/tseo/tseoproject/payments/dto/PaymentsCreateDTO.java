package tseo.tseoproject.payments.dto;

import lombok.Getter;
import lombok.Setter;
import tseo.tseoproject.payments.model.Payments;

@Getter
@Setter
public class PaymentsCreateDTO {

    private String bill;
    private String purpose;
    private Float amount;

    public PaymentsCreateDTO() {

    }

    public PaymentsCreateDTO(String bill, String purpose, Float amount) {
        super();
        this.bill = bill;
        this.purpose = purpose;
        this.amount = amount;
    }

    public Payments asEntity() {
        return new Payments(bill, purpose, amount);
    }


}
