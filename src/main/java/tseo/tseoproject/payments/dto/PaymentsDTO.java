package tseo.tseoproject.payments.dto;

import tseo.tseoproject.payments.model.Payments;

public class PaymentsDTO {
	private Long id;
	private String bill;
	private String purpose;
	private Float amount;
	
	public PaymentsDTO() {
		
	}

	public PaymentsDTO(Long id, String bill, String purpose, Float amount) {
	
		this.id = id;
		this.bill = bill;
		this.purpose = purpose;
		this.amount = amount;
	}
	public Payments asEntity() {
		return new Payments(id,bill,purpose,amount);
		
	}
	
	
	

}
