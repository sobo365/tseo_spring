package tseo.tseoproject.payments.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import tseo.tseoproject.payments.dto.PaymentsCreateDTO;
import tseo.tseoproject.payments.dto.PaymentsDTO;
import tseo.tseoproject.payments.model.Payments;
import tseo.tseoproject.payments.service.PaymentsService;


@RestController
@RequestMapping("/payments")
public class PaymentsController {

    private PaymentsService paymentsService;

    public PaymentsController(PaymentsService paymentsService) {
        this.paymentsService = paymentsService;
    }

    @GetMapping()
    public ResponseEntity<List<PaymentsDTO>> getAll() {
        List<Payments> payments = paymentsService.getAll();
        List<PaymentsDTO> paymenysDTOs = new ArrayList<>();

        for (Payments p : payments) {
            paymenysDTOs.add(p.asDTO());
        }
        return ResponseEntity.ok(paymenysDTOs);
    }

    @GetMapping("/{id}")
    public ResponseEntity<PaymentsDTO> get(@PathVariable Long id) throws Exception {
        Payments payments = paymentsService.get(id);
        return ResponseEntity.ok(payments.asDTO());
    }

    @PostMapping()
    public ResponseEntity<PaymentsDTO> insert(@RequestBody PaymentsCreateDTO paymentsCreateDTO) {
        Payments payments = paymentsService.insert(paymentsCreateDTO.asEntity());
        return ResponseEntity.ok(payments.asDTO());
    }

    @PutMapping()
    public ResponseEntity<PaymentsDTO> edit(@RequestBody PaymentsDTO paymenysDTO) {
        Payments payments = paymentsService.edit(paymenysDTO.asEntity());
        return ResponseEntity.ok(payments.asDTO());
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> delete(@PathVariable Long id) {
        paymentsService.delete(id);
        return ResponseEntity.ok("Payment deleted!");
    }

}
