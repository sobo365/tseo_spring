package tseo.tseoproject.payments.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Getter;
import lombok.ToString;
import tseo.tseoproject.payments.dto.PaymentsDTO;

@Getter
@ToString
@Entity
public class Payments {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String bill;
    private String purpose;
    private Float amount;


    public Payments() {
    }

    public Payments(Long id, String bill, String purpose, Float amount) {
        super();
        this.id = id;
        this.bill = bill;
        this.purpose = purpose;
        this.amount = amount;
    }


    public Payments(String bill, String purpose, Float amount) {

        this.bill = bill;
        this.purpose = purpose;
        this.amount = amount;
    }

    public PaymentsDTO asDTO() {
        return new PaymentsDTO(id, bill, purpose, amount);
    }

}