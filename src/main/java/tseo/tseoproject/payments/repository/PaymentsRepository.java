package tseo.tseoproject.payments.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.stereotype.Repository;
import tseo.tseoproject.payments.model.Payments;

@Repository
public interface PaymentsRepository extends JpaRepository<Payments, Long> {
}
