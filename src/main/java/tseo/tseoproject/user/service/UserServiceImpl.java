package tseo.tseoproject.user.service;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import tseo.tseoproject.security.JwtUtil;
import tseo.tseoproject.user.exception.UserNotFoundException;
import tseo.tseoproject.user.model.User;
import tseo.tseoproject.user.repository.UserRepository;

@Service
public class UserServiceImpl implements UserService {

    private AuthenticationManager authenticationManager;

    private JwtUtil jwtUtil;

    private UserDetailsServiceImpl userDetailsService;

    private UserRepository userRepository;

    public UserServiceImpl(AuthenticationManager authenticationManager, JwtUtil jwtUtil, UserDetailsServiceImpl userDetailsService, UserRepository userRepository) {
        this.authenticationManager = authenticationManager;
        this.jwtUtil = jwtUtil;
        this.userDetailsService = userDetailsService;
        this.userRepository = userRepository;
    }

    public String authenticate(String username, String password) {
        User user = userRepository.findByUsername(username);
        if (user == null)
            throw new UserNotFoundException("User not found!");
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(username, password, userDetailsService.getAuthority(user))
        );
        SecurityContextHolder.getContext().setAuthentication(authentication);
        final String token = jwtUtil.generateToken(authentication, user);
        return token;
    }

    public User getUserByUsername(String username) {
        User user = userRepository.findByUsername(username);
        return user;
    }

    @Override
    public User insert(User user) {
        return userRepository.save(user);
    }

}