package tseo.tseoproject.user.service.authority;

import org.springframework.stereotype.Service;
import tseo.tseoproject.user.model.Authority;
import tseo.tseoproject.user.repository.authority.AuthorityRepository;

@Service
public class AuthorityService {

    private AuthorityRepository authorityRepository;

    public AuthorityService(AuthorityRepository authorityRepository) {
        this.authorityRepository = authorityRepository;
    }

    public Authority getRoleStudent() {
        Authority authority = authorityRepository.getByName("ROLE_STUDENT");
        return authority;
    }

    public Authority getRoleTeacher() {
        Authority authority = authorityRepository.getByName("ROLE_TEACHER");
        return authority;
    }
}
