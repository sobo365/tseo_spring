package tseo.tseoproject.user.service.authority;

import org.springframework.stereotype.Service;
import tseo.tseoproject.user.model.UserAuthority;
import tseo.tseoproject.user.repository.authority.UserAuthorityRepository;

@Service
public class UserAuthorityService {

    private UserAuthorityRepository userAuthorityRepository;

    public UserAuthorityService(UserAuthorityRepository userAuthorityRepository) {
        this.userAuthorityRepository = userAuthorityRepository;
    }

    public UserAuthority insert(UserAuthority userAuthority){
        return userAuthorityRepository.save(userAuthority);
    }
}
