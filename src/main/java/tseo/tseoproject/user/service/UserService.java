package tseo.tseoproject.user.service;

import tseo.tseoproject.user.model.User;

public interface UserService {
    User insert(User user);
}
