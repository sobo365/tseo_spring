package tseo.tseoproject.user.util;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import tseo.tseoproject.student.model.Student;
import tseo.tseoproject.teacher.Teacher;
import tseo.tseoproject.user.model.Authority;
import tseo.tseoproject.user.model.User;
import tseo.tseoproject.user.model.UserAuthority;
import tseo.tseoproject.user.service.UserService;
import tseo.tseoproject.user.service.authority.AuthorityService;
import tseo.tseoproject.user.service.authority.UserAuthorityService;

import javax.transaction.Transactional;

@Component
public class UserGenerator {

    private UserService userService;
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    private AuthorityService authorityService;
    private UserAuthorityService userAuthorityService;

    public UserGenerator(UserService userService, BCryptPasswordEncoder bCryptPasswordEncoder, AuthorityService authorityService, UserAuthorityService userAuthorityService) {
        this.userService = userService;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.authorityService = authorityService;
        this.userAuthorityService = userAuthorityService;
    }

    @Transactional
    public void generateStudentAccount(Student student) {
        User user = new User(student.getCardNumber(), bCryptPasswordEncoder.encode(student.getLastName()), student.getId().toString());
        Authority authority = authorityService.getRoleStudent();
        UserAuthority userAuthority = new UserAuthority(user, authority);
        userAuthorityService.insert(userAuthority);
        userService.insert(user);
    }

    @Transactional
    public void generateTeacherAccount(Teacher teacher) {
        String username = teacher.getFirstName() + teacher.getLastName();
        User user = new User(username, bCryptPasswordEncoder.encode(teacher.getLastName()), teacher.getId());
        Authority authority = authorityService.getRoleTeacher();
        UserAuthority userAuthority = new UserAuthority(user, authority);
        userAuthorityService.insert(userAuthority);
        userService.insert(user);
    }
}
