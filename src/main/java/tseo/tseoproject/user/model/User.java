package tseo.tseoproject.user.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@Setter
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String username;

    private String password;

    private String personId;

    @OneToMany(mappedBy = "user", fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
    private Set<UserAuthority> userAuthorities = new HashSet<>();

    public User() {
    }

    public User(String username, String password, String personId) {
        this.username = username;
        this.password = password;
        this.personId = personId;
    }
}
