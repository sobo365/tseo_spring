package tseo.tseoproject.user.model;

public class UserDTO {
	private String username;
    private String password;
    private String personId;
    
    public UserDTO() {}

	public UserDTO(String username, String password, String personId) {
		this.username = username;
		this.password = password;
		this.personId = personId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPersonId() {
		return personId;
	}

	public void setPersonId(String personId) {
		this.personId = personId;
	}
}
