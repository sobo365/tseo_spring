package tseo.tseoproject.user.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@Setter
public class Authority {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    @OneToMany(mappedBy = "authority", fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
    private Set<UserAuthority> userAuthorities = new HashSet<>();

    public Authority() {
    }

    public Authority(String name, Set<UserAuthority> userAuthorities) {
        this.name = name;
        this.userAuthorities = userAuthorities;
    }
}
