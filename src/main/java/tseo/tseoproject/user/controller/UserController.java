package tseo.tseoproject.user.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import tseo.tseoproject.user.dto.LoginDTO;
import tseo.tseoproject.user.service.UserServiceImpl;

@RestController
@CrossOrigin
public class UserController {

    private UserServiceImpl userService;

    public UserController(UserServiceImpl userService) {
        this.userService = userService;
    }

    @PostMapping("/authenticate")
    public ResponseEntity<String> authenticate(@RequestBody LoginDTO loginDTO) {
        String token = userService.authenticate(loginDTO.getUsername(), loginDTO.getPassword());
        return ResponseEntity.ok(token);
    }
}
