package tseo.tseoproject.user.repository.authority;

import org.springframework.data.jpa.repository.JpaRepository;
import tseo.tseoproject.user.model.UserAuthority;

public interface UserAuthorityRepository extends JpaRepository<UserAuthority, Long> {
}
