package tseo.tseoproject.teacher.service;

import java.util.List;


import org.springframework.http.ResponseEntity;

import tseo.tseoproject.lecture.Lecture;
import tseo.tseoproject.teacher.Teacher;
import tseo.tseoproject.teacher.dto.TeacherDTO;
import tseo.tseoproject.user.model.User;

public interface TeacherService {
	List<Teacher> getAll();
	Teacher get(String id) throws Exception;
	Teacher edit(Teacher teacher);
    Teacher insert(Teacher teacher);
    void delete(String id);
    User editUser(User user);
}
