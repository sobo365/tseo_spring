package tseo.tseoproject.teacher.service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.stereotype.Service;
import tseo.tseoproject.teacher.Teacher;
import tseo.tseoproject.teacher.exception.TeacherNotFoundException;
import tseo.tseoproject.teacher.repository.TeacherRepository;
import tseo.tseoproject.user.model.User;
import tseo.tseoproject.user.repository.UserRepository;
import tseo.tseoproject.user.util.UserGenerator;

@Service
public class TeacherServiceImpl implements TeacherService {

    private TeacherRepository teacherRepository;
    private UserRepository userRepository;
    private UserGenerator userGenerator;

    public TeacherServiceImpl(TeacherRepository teacherRepository, UserRepository userRepository, UserGenerator userGenerator) {
        this.teacherRepository = teacherRepository;
        this.userRepository = userRepository;
        this.userGenerator = userGenerator;
    }

    @Override
    public List<Teacher> getAll() {
        return teacherRepository.findAllByActive(true);
    }

    @Override
    public Teacher get(String id) {
        Optional<Teacher> teacher = teacherRepository.findById(id);
        if (!teacher.isPresent() || !teacher.get().isActive())
            throw new TeacherNotFoundException("Teacher with provided id does not exists!");
        return teacher.get();
    }

    @Override
    public Teacher insert(Teacher teacher) {
        teacher.setId(UUID.randomUUID().toString());
        teacher.setActive(true);
        Teacher t = teacherRepository.save(teacher);
        userGenerator.generateTeacherAccount(t);
        return t;
    }

    @Override
    public Teacher edit(Teacher teacher) {
        teacher.setActive(true);
        if (!teacherRepository.existsById(teacher.getId().toString()))
            throw new TeacherNotFoundException("Teacher with provided id does not exists!");
        return teacherRepository.save(teacher);
    }

    @Override
    public void delete(String id) {
        Optional<Teacher> teacher = teacherRepository.findById(id);
        if (!teacherRepository.existsById(id))
            throw new TeacherNotFoundException("Teacher with provided id does not exists!");
        teacher.get().delete();
        teacherRepository.save(teacher.get());
    }

    @Override
    public User editUser(User user) {
        if (!userRepository.existsById(user.getId()))
            throw new TeacherNotFoundException("User with provided id does not exists!");
        return userRepository.save(user);
    }

}
