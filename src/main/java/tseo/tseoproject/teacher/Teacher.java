package tseo.tseoproject.teacher;

import java.util.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.Type;
import tseo.tseoproject.course.dto.CourseDTO;
import tseo.tseoproject.enrollment.model.Enrollment;
import tseo.tseoproject.lecture.Lecture;
import tseo.tseoproject.lecture.dto.LectureDTO;
import tseo.tseoproject.teacher.dto.TeacherDTO;

@Getter
@Entity
@ToString
@Setter
public class Teacher {

    @Id
    private String id;
    private String firstName;
    private String lastName;
    private boolean active;

    @OneToMany(mappedBy = "teachers")
    private Set<Lecture> lectures = new HashSet<>();

    @OneToMany(mappedBy = "teacher")
    private Set<Enrollment> enrollments = new HashSet<>();

    public Teacher() {
    }

    public Teacher(String id, String firstName, String lastName, boolean active) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.active = active;
    }

    public Teacher(String id, String firstName, String lastName) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public Teacher(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public void delete() {
        this.setActive(false);
    }

    public TeacherDTO asDTO() {
        return new TeacherDTO(id.toString(), firstName, lastName);
    }
}
