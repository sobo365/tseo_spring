package tseo.tseoproject.teacher.dto;

import lombok.Getter;
import tseo.tseoproject.teacher.Teacher;

@Getter
public class TeacherCreateDTO {
    private String firstName;
    private String lastName;

    public TeacherCreateDTO() {

    }

    public TeacherCreateDTO(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public Teacher asEntity() {
        return new Teacher(firstName, lastName);
    }
}
