package tseo.tseoproject.teacher.dto;

import lombok.Getter;
import tseo.tseoproject.teacher.Teacher;

@Getter
public class TeacherDTO {

    private String id;
    private String firstName;
    private String lastName;

    public TeacherDTO() {
    }

    public TeacherDTO(String id, String firstName, String lastName) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public Teacher asEntity() {
        return new Teacher(id, firstName, lastName);
    }
}
