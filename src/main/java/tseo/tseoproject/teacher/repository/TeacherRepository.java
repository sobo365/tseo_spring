package tseo.tseoproject.teacher.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import tseo.tseoproject.lecture.Lecture;
import tseo.tseoproject.teacher.Teacher;

import java.util.List;

@Repository
public interface TeacherRepository extends JpaRepository<Teacher, String>{
    List<Teacher> findAllByActive(boolean active);
}
