package tseo.tseoproject.teacher.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.fasterxml.jackson.databind.ObjectMapper;

import tseo.tseoproject.lecture.Lecture;
import tseo.tseoproject.lecture.dto.LectureDTO;
import tseo.tseoproject.security.JwtUtil;
import tseo.tseoproject.teacher.Teacher;
import tseo.tseoproject.teacher.dto.TeacherCreateDTO;
import tseo.tseoproject.teacher.dto.TeacherDTO;
import tseo.tseoproject.teacher.service.TeacherService;
import tseo.tseoproject.user.model.User;
import tseo.tseoproject.user.model.UserDTO;
import tseo.tseoproject.user.repository.UserRepository;
import tseo.tseoproject.user.service.UserDetailsServiceImpl;
import tseo.tseoproject.user.service.UserService;
import tseo.tseoproject.user.service.UserServiceImpl;

@RestController
@RequestMapping("/teachers")
@CrossOrigin(origins = "http://localhost:4200")
public class TeacherController {
	
	private TeacherService teacherService;
	private UserServiceImpl userService;
	
	@Autowired
    private UserDetailsServiceImpl userDetailsService;

	public TeacherController(TeacherService teacherService, UserServiceImpl userService) {
		this.teacherService = teacherService;
		this.userService = userService;
	}
	
	@GetMapping()
    public ResponseEntity<List<TeacherDTO>> getAll() {
        List<Teacher> teachers = teacherService.getAll();
        List<TeacherDTO> teacherDTOs = new ArrayList<>();

        for (Teacher t : teachers) {
            teacherDTOs.add(t.asDTO());
        }
        return ResponseEntity.ok(teacherDTOs);
    }
	
	@GetMapping("/{id}")
    public ResponseEntity<TeacherDTO> get(@PathVariable String id) throws Exception {
        Teacher teacher = teacherService.get(id);
        return ResponseEntity.ok(teacher.asDTO());
    }

    @GetMapping("/lectures/{id}")
    public ResponseEntity<List<LectureDTO>> getLectures(@PathVariable String id) throws Exception {
	    Teacher teacher = teacherService.get(id);
	    List<LectureDTO> dtos = new ArrayList<>();
	    teacher.getLectures().stream().forEach(l -> {if (l.isActive()) dtos.add(l.asDTO());});
	    return ResponseEntity.ok(dtos);
    }

    @PostMapping()
    public ResponseEntity<TeacherDTO> insert(@RequestBody TeacherCreateDTO teacherCreateDTO) {
        Teacher teacher = teacherService.insert(teacherCreateDTO.asEntity());
        return ResponseEntity.ok(teacher.asDTO());
    }

    @PutMapping()
    public ResponseEntity<TeacherDTO> edit(@RequestBody TeacherDTO teacherDTO) {
        Teacher teacher = teacherService.edit(teacherDTO.asEntity());
        return ResponseEntity.ok(teacher.asDTO());
    }
    
    @PutMapping("/editUser")
    public String editUser(@RequestHeader("Authorization") String rawToken, @RequestBody UserDTO userDTO) {
    	// remove Bearer from the beginning of the token
    	String token = rawToken.substring(6);
    			
    	// load username from token
    	JwtUtil jwtUtil = new JwtUtil();
    	String username = jwtUtil.getUsernameFromToken(token);
    	System.out.println("Ovo je dobijeni username:");
    	System.out.println(username);
    	
    	// hash password 
    	PasswordEncoder encoder = new BCryptPasswordEncoder(12);
        String hashedPassword = encoder.encode(userDTO.getPassword());
    			
    	// load user from username
    	User user = userService.getUserByUsername(username);
    	user.setUsername(userDTO.getUsername());
    	user.setPassword(hashedPassword);
    	
    	// save user
    	if (teacherService.editUser(user) != null) {
    		return "{ \"status\": \"success\" }";
    	} else {
    		return "{ \"status\": \"failed\" }";
    	}
    }
	
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable String id) {
        teacherService.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
    
    @GetMapping("/myTeacherAccount")
    public ResponseEntity<TeacherDTO> getTeacherAccountDetails(@RequestHeader("Authorization") String rawToken) throws Exception {
		System.out.println(rawToken);
		
		// remove Bearer from the begining of the token
		String token = rawToken.substring(6);
		
		// load username from token
		JwtUtil jwtUtil = new JwtUtil();
		String username = jwtUtil.getUsernameFromToken(token);
		System.out.println("Ovo je dobijeni username:");
		System.out.println(username);
		
		// load user from username
		User user = userService.getUserByUsername(username);
		
        Teacher teacher = teacherService.get(user.getPersonId());
        
        return ResponseEntity.ok(teacher.asDTO());
    }
    
    @RequestMapping(value = "/myUserAccount", method = RequestMethod.GET, produces="application/json")
    public String getUserAccountDetails(@RequestHeader("Authorization") String rawToken) throws Exception {
		System.out.println(rawToken);
		
		// remove Bearer from the begining of the token
		String token = rawToken.substring(6);
		
		// load username from token
		JwtUtil jwtUtil = new JwtUtil();
		String username = jwtUtil.getUsernameFromToken(token);
		System.out.println("Ovo je dobijeni username:");
		System.out.println(username);
		
		// load user from username
		User user = userService.getUserByUsername(username);
		
		HashMap<String, String> userValues = new HashMap<String, String>();
		userValues.put("id", user.getId().toString());
		userValues.put("username", username);
		userValues.put("person_id", user.getPersonId());
        
        ObjectMapper mapper = new ObjectMapper();
        String customResponse = mapper.writeValueAsString(userValues);
        
        return customResponse;
    }
}
